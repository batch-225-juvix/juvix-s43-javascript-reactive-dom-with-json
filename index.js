
let posts = []; // storage
let count = 1; // id
//  add post data
document.querySelector("#form-add-post");
addEventListener("submit", (e) => {
  e.preventDefault();
  posts.push({
    id: count,
    title: document.querySelector("#txt-title").value,
    body: document.querySelector("#txt-body").value,
  });
  count++; //nag loop, for formality
  console.log(posts);
  showPosts(posts);
  alert("Successfully Added");
});
// show your input to POSTS
const showPosts = (posts) => {
  let postEntries = "";
  posts.forEach((post) => {
    postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id= "post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')"> Edit </bottun>
                <button onclick="deletePost('${post.id}')"> Delete </bottun> 
             </div>`;
  });
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};
// Edit Post
const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
};
// Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();
  for (let i = 0; i < posts.length; i++) {
    // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
    // Therefore, it is necessary to convert the Number to a String
    if (
      posts[i].id.toString() === document.querySelector("#txt-edit-id").value
    ) {
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;
    }
  }
  showPosts(posts);
  alert("Succesfully updated");
});
// delete post
const deletePost = (id) => {
  for (let i = 0; i < posts.length; i++) {
    if (posts[i].id === parseInt(id)) {
      posts.splice(i, 1);
      break;
    }
  }
  showPosts(posts);
  alert("Successfully Deleted");
};
